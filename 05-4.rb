# Your code here!
#蔵書管理アプリ(データベース版)

require "date"
require "dbi"

#蔵書データをクラスに入れてまとめる（その３）
class BookInfos
  attr_accessor :title, :author, :pages, :publish_date

  def initialize(title, author, pages, publish_date)
    @title = title
    @author = author
    @pages = pages
    @publish_date = publish_date
  end

  def to_s
    "#{@title},#{@author}, #{@pages}, #{@publish_date}"
  end

  def toFormattedString()
    puts "書籍名：" + @title
    puts "著者名：" + @author
    puts "ページ数：" + @pages.to_s
    puts "発刊日：" + @publish_date.to_s
  end
end

class BookInfoManager
  def initialize(db_fname)
    @db_fname = db_fname
    @dbh = DBI.connect("DBI:SQLite3:#{@db_fname}")
    @dbh.do("create table if not exists bookinfos(
     title         varchar(50),
     author        varchar(50),
     pages         int,
     publish_date  date);")
  end

  def addBookInfo
    book_info = BookInfos.new("", "", 0, Date.new)
    print "書籍名："
    book_info.title = gets.chomp
    print "著者名："
    book_info.author = gets.chomp
    print "ページ数："
    book_info.pages = gets.chomp.to_i
    print "発刊年："
    year = gets.chomp.to_i
    print "発刊月："
    month = gets.chomp.to_i
    print "発刊日："
    day = gets.chomp.to_i
    book_info.publish_date = Date.new(year, month, day)

    @dbh.do("insert into bookinfos values (
      \'#{book_info.title}\',
      \'#{book_info.author}\',
      #{book_info.pages},
      \'#{book_info.publish_date}\');")
  end

  def listAllBookInfos
    @dbh.select_all("select * from bookinfos") do |row|
      puts BookInfos.new(row[:title], row[:author], row[:pages], row[:publish_date]).toFormattedString
    end
  end

  def run
    while true

      #メニュー表示(puts)
      print "
1. 蔵書データの登録
2. 蔵書データの表示
9. 終了
番号を選んでください（1,2,9）:
"

      #メニュー番号受付（gets.chomp）
      num = gets.chomp

      #メニュー番号に応じた処理を実行する（case / when）
      case
      when "1" == num
        #登録処理
        addBookInfo
      when "2" == num
        #表示処理
        listAllBookInfos
      when "9" == num
        break
      end
      #１番に戻る
    end
    @dbh.disconnect
  end
end

bim = BookInfoManager.new("book_info.db")
bim.run
