# Your code here!
#蔵書管理アプリ(コンソール版)

require "date"


#蔵書データをクラスに入れてまとめる（その３）
class BookInfos
  attr_accessor :title, :author, :pages, :publish_date
  def initialize(title, author, pages, publish_date)
    @title = title
    @author = author
    @pages = pages
    @publish_date = publish_date
  end

  def to_s
    "#{@title},#{@author}, #{@pages}, #{@publish_date}"
  end

  def toFormattedString()
    puts "書籍名：" + @title
    puts "著者名：" + @author
    puts "ページ数：" + @pages.to_s
    puts "発刊日：" + @publish_date.to_s
  end
end

class BookInfoManager
  def initialize(filename)
    @csv_file = filename
    @book_infos = []
  end
  
  def setUp
    @csv_file = "book_info.csv"
    
    if File.exist?(@csv_file)
      open(@csv_file, "r:UTF-8") {|file|
      file.each {|line|
        title, author, pages, publish_date = line.chomp.split(",")
      @book_info << BookInfo.new(title, author, pages.to_i, Date.parse(publish_date))
        }
      }
    end
  end

  def addBookInfo
    book_info = BookInfos.new("", "", 0, Date.new)
    print "書籍名："
    book_info.title = gets.chomp
    print "著者名："
    book_info.author = gets.chomp
    print "ページ数："
    book_info.pages = gets.chomp.to_i
    print "発刊年："
    year = gets.chomp.to_i
    print "発刊月："
    month = gets.chomp.to_i
    print "発刊日："
    day = gets.chomp.to_i
    book_info.publish_date = Date.new(year, month, day)

   @book_infos << book_info
  end

  def listAllBookInfos
   @book_infos.each { |info|
   print info.toFormattedString
   }
  end
  
  def saveAllBookInfos
    open(@csv_file, "w:UTF-8") {|file|
    @book_infos.each {|book|
    file.puts book.to_s
    }
    puts "\nファイルへ保存しました."
    bool = 0
    }
  end

  def run
    while true

      #メニュー表示(puts)
      print "
0.蔵書データベースの初期化
1. 蔵書データの登録
2. 蔵書データの表示
8. 蔵書データをファイルへ保存
9. 終了
番号を選んでください（0,1,2,8,9）:
"

      #メニュー番号受付（gets.chomp）
      num = gets.chomp

      #メニュー番号に応じた処理を実行する（case / when）
      case
      when "0" == num
        #初期化処理
        initialize
      when "1" == num
        #登録処理
        addBookInfo
      when "2" == num
        #表示処理
        listAllBookInfos
        when "8" == num
        #ファイルへ保存
        saveAllBookInfos
        bool = 0
      when "9" == num
        if bool != 0
          print"登録した蔵書データが保存されていません.
          終了してもよろしいですか？(Y/N):"
          num = gets.chomp
          case
          when 'Y' == num
            break
          when 'N' == num
          end
          else
            break
        end
      end
      #１番に戻る
    end
  end
end

bim = BookInfoManager.new("book_info.csv")
bim.run
